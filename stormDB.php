<?php

Class StormDatabase  {

	public function __construct($host, $user, $pass, $db_name) {
		$this->host = $host;
		$this->user = $user;
		$this->pass = $pass;
		$this->db_name = $db_name;
	}

	protected function connect() {
		try {
			return new mysqli($this->host, $this->user, $this->pass, $this->db_name);
		} catch (mysqli_sql_exception $e) {
			// App->errorHandle($e);
			die('Unable to connect');
		}
	}

	public function query($query) {
		$db = $this->connect();
		$returned = $db->query($query);

		while($result = $returned->fetch_object()) {
			$results[] = $result;
		}

		return $results;
	}

	public function select($table, $limit = '*', $selector, $value) {
		$db = $this->connect();

		$stmt = $db->prepare("SELECT ? FROM {$table} WHERE {$selector} = ?")

		$stmt->bind_param('ss', $limit, $value);
		$stmt->execute();
		$returned = $stmt->get_result();


		while($result = $returned->fetch_object()) {
			$results[] = $result;
		}

		return $results;
	}
	
}

?>